// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "album_view_placeholder_search":
            MessageLookupByLibrary.simpleMessage("Search"),
        "current_language": MessageLookupByLibrary.simpleMessage("en"),
        "details_view_bookmark_now":
            MessageLookupByLibrary.simpleMessage("Bookmark now!!"),
        "details_view_do_you_like_it_and_ask_for_bookmark":
            MessageLookupByLibrary.simpleMessage("Do you like it?"),
        "general_error": MessageLookupByLibrary.simpleMessage("Error"),
        "main_view_app_bar_title_albums":
            MessageLookupByLibrary.simpleMessage("Search for albums"),
        "main_view_app_bar_title_bookmarks":
            MessageLookupByLibrary.simpleMessage("Swipe to delete"),
        "main_view_bottom_navigation_item_title_albums":
            MessageLookupByLibrary.simpleMessage("Albums"),
        "main_view_bottom_navigation_item_title_bookmark":
            MessageLookupByLibrary.simpleMessage("Bookmark"),
        "main_view_bottom_navigation_item_title_settings":
            MessageLookupByLibrary.simpleMessage("Settings"),
        "settings_view_dark_mode":
            MessageLookupByLibrary.simpleMessage("Dark Mode")
      };
}
