// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `en`
  String get current_language {
    return Intl.message(
      'en',
      name: 'current_language',
      desc: '',
      args: [],
    );
  }

  /// `Search for albums`
  String get main_view_app_bar_title_albums {
    return Intl.message(
      'Search for albums',
      name: 'main_view_app_bar_title_albums',
      desc: '',
      args: [],
    );
  }

  /// `Swipe to delete`
  String get main_view_app_bar_title_bookmarks {
    return Intl.message(
      'Swipe to delete',
      name: 'main_view_app_bar_title_bookmarks',
      desc: '',
      args: [],
    );
  }

  /// `Albums`
  String get main_view_bottom_navigation_item_title_albums {
    return Intl.message(
      'Albums',
      name: 'main_view_bottom_navigation_item_title_albums',
      desc: '',
      args: [],
    );
  }

  /// `Bookmark`
  String get main_view_bottom_navigation_item_title_bookmark {
    return Intl.message(
      'Bookmark',
      name: 'main_view_bottom_navigation_item_title_bookmark',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get main_view_bottom_navigation_item_title_settings {
    return Intl.message(
      'Settings',
      name: 'main_view_bottom_navigation_item_title_settings',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get album_view_placeholder_search {
    return Intl.message(
      'Search',
      name: 'album_view_placeholder_search',
      desc: '',
      args: [],
    );
  }

  /// `Dark Mode`
  String get settings_view_dark_mode {
    return Intl.message(
      'Dark Mode',
      name: 'settings_view_dark_mode',
      desc: '',
      args: [],
    );
  }

  /// `Do you like it?`
  String get details_view_do_you_like_it_and_ask_for_bookmark {
    return Intl.message(
      'Do you like it?',
      name: 'details_view_do_you_like_it_and_ask_for_bookmark',
      desc: '',
      args: [],
    );
  }

  /// `Bookmark now!!`
  String get details_view_bookmark_now {
    return Intl.message(
      'Bookmark now!!',
      name: 'details_view_bookmark_now',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get general_error {
    return Intl.message(
      'Error',
      name: 'general_error',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
