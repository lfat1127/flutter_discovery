import 'package:flutter/material.dart';
import 'package:flutter_experience/app.dart';
import 'package:flutter_experience/managers/language_manager.dart';
import 'package:flutter_experience/managers/user_settings_manager.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LanguageManager().ensureInitialized();
  await UserSettingsManager().ensureInitialized();

  runApp(
    const FlutterExperienceApp(),
  );
}
