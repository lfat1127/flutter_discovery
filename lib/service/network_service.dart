import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_experience/exceptions/network_exception.dart';

class NetworkService {
  static final NetworkService _singleton = NetworkService._internal();

  factory NetworkService() {
    return _singleton;
  }

  NetworkService._internal();
  static String baseUrl = "https://itunes.apple.com";

  final _dio = Dio(
    BaseOptions(baseUrl: baseUrl),
  );
  final successCode = 200;

  Future getJsonResponse(String path) async {
    final response = await _dio.get(path);
    if (response.statusCode == successCode) {
      return jsonDecode(response.data);
    } else {
      throw NetworkException(response.statusCode);
    }
  }
}
