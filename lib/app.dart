import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_experience/bloc/details/details_bloc.dart';
import 'package:flutter_experience/bloc/main/bookmarked/bookmarked_bloc.dart';
import 'package:flutter_experience/bloc/settings/settings_bloc.dart';
import 'package:flutter_experience/generated/l10n.dart';
import 'package:flutter_experience/managers/language_manager.dart';
import 'package:flutter_experience/managers/user_settings_manager.dart';
import 'package:flutter_experience/repository/itunes_search_repository.dart';
import 'package:flutter_experience/repository/local_db_repository.dart';
import 'package:flutter_experience/router/flutter_experience_router.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'bloc/main/albums/albums_bloc.dart';

class FlutterExperienceApp extends StatefulWidget {
  const FlutterExperienceApp({super.key});
  @override
  State<StatefulWidget> createState() => _FlutterExperienceAppState();
}

class _FlutterExperienceAppState extends State<FlutterExperienceApp> {
  Locale _currentLanguage = LanguageManager().currentLanguage.value;
  StreamSubscription? currentLanguageStreamSubscription;
  StreamSubscription? themeModeStreamSubscription;
  ThemeMode _themeMode = ThemeMode.system;
  @override
  void initState() {
    currentLanguageStreamSubscription =
        LanguageManager().currentLanguage.listen((value) {
      setState(() {
        _currentLanguage = value;
      });
    });
    themeModeStreamSubscription =
        UserSettingsManager().themeModeSubject.listen((value) {
      setState(() {
        _themeMode = value;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    currentLanguageStreamSubscription?.cancel();
    themeModeStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<ItunesSearchRepository>(
            create: (context) => ItunesSearchRepository()),
        RepositoryProvider<LocalDatabaseRepository>(
            create: (context) => LocalDatabaseRepository())
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AlbumsBloc>(
            create: (context) => AlbumsBloc(
              itunesSearchRepository: RepositoryProvider.of(context),
              localDatabaseRepository: RepositoryProvider.of(context),
            ),
          ),
          BlocProvider<BookmarkedBloc>(
            create: (context) => BookmarkedBloc(
              localDatabaseRepository: RepositoryProvider.of(context),
            ),
          ),
          BlocProvider<DetailsBloc>(
            create: (context) => DetailsBloc(
              localDatabaseRepository: RepositoryProvider.of(context),
            ),
          ),
          BlocProvider<SettingsBloc>(
            create: (context) => SettingsBloc(),
          ),
        ],
        child: MaterialApp.router(
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          locale: _currentLanguage,
          routerConfig: flutterExperienceRouter,
          theme: ThemeData(),
          darkTheme: ThemeData.dark(),
          themeMode: _themeMode,
        ),
      ),
    );
  }
}
