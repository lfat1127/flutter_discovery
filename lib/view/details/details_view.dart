import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_experience/bloc/details/details_bloc.dart';
import 'package:flutter_experience/generated/l10n.dart';
import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:go_router/go_router.dart';

class DetailsView extends StatefulWidget {
  final AlbumsData? albumsData;
  const DetailsView({super.key, required this.albumsData});

  @override
  State<DetailsView> createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    context
        .read<DetailsBloc>()
        .add(InitialiseEvent(albumsData: widget.albumsData));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<DetailsBloc, DetailsState>(builder: (context, state) {
      return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () => context.pop(),
              icon: const Icon(Icons.close),
            ),
            actions: [
              if (widget.albumsData != null)
                IconButton(
                    onPressed: () {
                      context
                          .read<DetailsBloc>()
                          .add(const BookmarkTappedEvent());
                    },
                    icon: state.isBookmarked
                        ? const Icon(Icons.bookmark)
                        : const Icon(Icons.bookmark_border_outlined))
            ],
          ),
          body: widget.albumsData == null
              ? Text(S.of(context).general_error)
              : _AlbumDetailsView(
                  albumsData: widget.albumsData!,
                ));
    });
  }
}

class _AlbumDetailsView extends StatelessWidget {
  final AlbumsData albumsData;

  const _AlbumDetailsView({required this.albumsData});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(
        width: double.infinity,
        height: 100,
        child: Row(
          children: [
            CachedNetworkImage(
              imageUrl: albumsData.artworkUrl100 ?? "",
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(
                      albumsData.collectionName ?? "",
                      style: const TextStyle(fontSize: 20),
                      maxLines: 2,
                    ),
                    AutoSizeText(
                      albumsData.artistName ?? "",
                      style: const TextStyle(fontSize: 20),
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
