import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_experience/bloc/settings/settings_bloc.dart';
import 'package:flutter_experience/generated/l10n.dart';

class SettingsView extends StatefulWidget {
  const SettingsView({super.key});

  @override
  State<StatefulWidget> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    context.read<SettingsBloc>().add(const InitialiseEvent());
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, state) {
        return ListView(
          children: [
            ListTile(
              title: Text(S.of(context).settings_view_dark_mode),
              trailing: Switch.adaptive(
                  value: state.isDarkMode,
                  onChanged: (value) {
                    context
                        .read<SettingsBloc>()
                        .add(const ThemeModeSwitchedEvent());
                  }),
            )
          ],
        );
      },
    );
  }
}
