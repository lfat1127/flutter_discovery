import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_experience/bloc/main/bookmarked/bookmarked_bloc.dart';
import 'package:flutter_experience/extensions/string_extension.dart';

class BookmarkedView extends StatefulWidget {
  const BookmarkedView({super.key});

  @override
  State<BookmarkedView> createState() => _BookmarkedViewState();
}

class _BookmarkedViewState extends State<BookmarkedView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    context.read<BookmarkedBloc>().add(const InitialiseEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<BookmarkedBloc, BookmarkedState>(
        builder: (context, state) {
      // prevent any data is invalid
      final albumDatasInRealm =
          state.albumDatasInRealm.where((element) => element.isValid).toList();
      return ListView.separated(
          itemBuilder: (context, index) {
            final albumData = albumDatasInRealm.elementAt(index);
            return Dismissible(
              // Each Dismissible must contain a Key. Keys allow Flutter to
              // uniquely identify widgets.
              key: Key(albumData.collectionViewUrl.returnEmptyIfNull()),
              // Provide a function that tells the app
              // what to do after an item has been swiped away.
              onDismissed: (direction) {
                // Remove the item from the data source.
                context.read<BookmarkedBloc>().add(RemoveBookmarkEvent(
                    collectionViewUrl: albumData.collectionViewUrl!));
              },
              direction: DismissDirection.endToStart,
              // Show a red background as the item is swiped away.
              background: Container(color: Colors.red),
              child: ListTile(
                title: Text(albumData.collectionName ??
                    albumData.artistName.returnEmptyIfNull()),
                onTap: () {},
              ),
            );
          },
          separatorBuilder: (context, index) => const Divider(),
          itemCount: albumDatasInRealm.length);
    });
  }
}
