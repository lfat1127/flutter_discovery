import 'package:flutter/material.dart';
import 'package:flutter_experience/generated/l10n.dart';
import 'package:flutter_experience/managers/language_manager.dart';
import 'package:flutter_experience/view/main/albums/albums_view.dart';
import 'package:flutter_experience/view/main/bookmarked/bookmarked_view.dart';
import 'package:flutter_experience/view/settings/settings_view.dart';

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  final bodyViews = const <Widget>[
    AlbumsView(),
    BookmarkedView(),
    SettingsView(),
  ];

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final bottomNavigationBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: const Icon(Icons.library_music),
        label: S.of(context).main_view_bottom_navigation_item_title_albums,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.bookmark_added),
        label: S.of(context).main_view_bottom_navigation_item_title_bookmark,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.settings),
        label: S.of(context).main_view_bottom_navigation_item_title_settings,
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(_currentIndex == 0
            ? S.of(context).main_view_app_bar_title_albums
            : S.of(context).main_view_app_bar_title_bookmarks),
        actions: [
          IconButton(
              onPressed: () {
                final currentLanguage = S.of(context).current_language;
                LanguageManager().changeLanguageTo(
                    context: context, currentLanguageCode: currentLanguage);
              },
              icon: const Icon(Icons.language))
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: bottomNavigationBarItems,
        currentIndex: _currentIndex,
        onTap: (value) {
          setState(() {
            _currentIndex = value;
          });
        },
      ),
      body: bodyViews.elementAt(_currentIndex),
    );
  }
}
