import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_experience/bloc/main/albums/albums_bloc.dart';
import 'package:flutter_experience/extensions/string_extension.dart';
import 'package:flutter_experience/generated/l10n.dart';
import 'package:flutter_experience/repository/local_db_repository.dart';
import 'package:go_router/go_router.dart';
import 'package:rxdart/rxdart.dart';

class AlbumsView extends StatefulWidget {
  const AlbumsView({super.key});

  @override
  State<AlbumsView> createState() => _AlbumsViewState();
}

class _AlbumsViewState extends State<AlbumsView>
    with AutomaticKeepAliveClientMixin {
  StreamSubscription? localDatabaseChangesEventSubscription;
  final PublishSubject<String> onTextChanged = PublishSubject();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    context.read<AlbumsBloc>().add(const InitialiseEvent());
    localDatabaseChangesEventSubscription =
        LocalDatabaseRepository.localDatabaseChangesEvent.listen((value) {
      context.read<AlbumsBloc>().add(const LocalDatabaseChangesEvent());
    });
    onTextChanged
        .distinct()
        .debounceTime(const Duration(milliseconds: 500))
        .listen((keyword) {
      context.read<AlbumsBloc>().add(SearchTextChangesEvent(keywords: keyword));
    });
    super.initState();
  }

  @override
  void dispose() {
    localDatabaseChangesEventSubscription?.cancel();
    onTextChanged.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<AlbumsBloc, AlbumsState>(
      builder: (context, state) {
        return Column(
          children: [
            if (state.isLoading)
              const CircularProgressIndicator()
            else ...[
              TextField(
                decoration: InputDecoration(
                  hintText: S.of(context).album_view_placeholder_search,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 8),
                ),
                onChanged: (value) => onTextChanged.add(value),
              ),
              Expanded(
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      final albumData = state.albumDatas.elementAt(index);
                      return ListTile(
                        title: Text(albumData.collectionName ??
                            albumData.artistName.returnEmptyIfNull()),
                        trailing: IconButton(
                          onPressed: () {
                            context.read<AlbumsBloc>().add(
                                BookmarkButtonTappedEvent(
                                    albumData: albumData));
                          },
                          icon: state.bookmarkedCollectionCollectionViewUrl
                                  .contains(albumData.collectionViewUrl)
                              ? const Icon(Icons.bookmark_outlined)
                              : const Icon(Icons.bookmark_border),
                        ),
                        onTap: () {
                          context.push('/details', extra: albumData);
                        },
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(),
                    itemCount: state.albumDatas.length),
              ),
            ]
          ],
        );
      },
    );
  }
}
