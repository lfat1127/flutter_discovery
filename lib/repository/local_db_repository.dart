import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:realm/realm.dart';
import 'package:rxdart/rxdart.dart';

class LocalDatabaseRepository {
  final config = Configuration.local([AlbumsDataInRealm.schema]);
  late Realm _realm;
  static final localDatabaseChangesEvent = PublishSubject<bool>();

  LocalDatabaseRepository() {
    _realm = Realm(config);
  }

  RealmResults<AlbumsDataInRealm> getSavedAlbums() {
    return _realm.all<AlbumsDataInRealm>();
  }

  saveToLocal(AlbumsData albumsData) {
    _realm.write(() {
      _realm.add<AlbumsDataInRealm>(albumsData.convertToRealmObject());
    });
    localDatabaseChangesEvent.add(true);
  }

  removeFromLocal(String? collectionViewUrl) {
    if (collectionViewUrl != null && collectionViewUrl.isNotEmpty) {
      var storedObject = _realm.query<AlbumsDataInRealm>(
          r"collectionViewUrl == $0", [collectionViewUrl]);
      if (storedObject.isNotEmpty) {
        _realm.write(() {
          _realm.delete<AlbumsDataInRealm>(storedObject.first);
        });
      }
      localDatabaseChangesEvent.add(true);
    }
  }

  closeRealm() {
    _realm.close();
  }
}
