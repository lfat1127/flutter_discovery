import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:flutter_experience/models/response/itunes_search_response/itunes_search_response.dart';
import 'package:flutter_experience/service/network_service.dart';

class ItunesSearchRepository {
  final _networkService = NetworkService();

  Future<List<AlbumsData>> searchAlbum(
      {required String term, String entity = "album"}) async {
    if (term.isEmpty) {
      return Future.value([]);
    }
    try {
      final result = await _networkService
          .getJsonResponse('/search?term={$term}&entity=$entity');
      return ItunesSearchResponse.fromJson(result).results;
    } catch (exceptions) {
      return Future.value([]);
    }
  }
}
