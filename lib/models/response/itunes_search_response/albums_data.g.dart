// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'albums_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlbumsData _$AlbumsDataFromJson(Map<String, dynamic> json) => AlbumsData(
      json['wrapperType'] as String?,
      json['collectionType'] as String?,
      json['artistId'] as int?,
      json['collectionId'] as int?,
      json['amgArtistId'] as int?,
      json['artistName'] as String?,
      json['collectionName'] as String?,
      json['collectionCensoredName'] as String?,
      json['artistViewUrl'] as String?,
      json['collectionViewUrl'] as String?,
      json['artworkUrl60'] as String?,
      json['artworkUrl100'] as String?,
      (json['collectionPrice'] as num?)?.toDouble(),
      json['collectionExplicitness'] as String?,
      json['trackCount'] as int?,
      json['copyright'] as String?,
      json['country'] as String?,
      json['currency'] as String?,
      json['releaseDate'] == null
          ? null
          : DateTime.parse(json['releaseDate'] as String),
      json['primaryGenreName'] as String?,
    );

Map<String, dynamic> _$AlbumsDataToJson(AlbumsData instance) =>
    <String, dynamic>{
      'wrapperType': instance.wrapperType,
      'collectionType': instance.collectionType,
      'artistId': instance.artistId,
      'collectionId': instance.collectionId,
      'amgArtistId': instance.amgArtistId,
      'artistName': instance.artistName,
      'collectionName': instance.collectionName,
      'collectionCensoredName': instance.collectionCensoredName,
      'artistViewUrl': instance.artistViewUrl,
      'collectionViewUrl': instance.collectionViewUrl,
      'artworkUrl60': instance.artworkUrl60,
      'artworkUrl100': instance.artworkUrl100,
      'collectionPrice': instance.collectionPrice,
      'collectionExplicitness': instance.collectionExplicitness,
      'trackCount': instance.trackCount,
      'copyright': instance.copyright,
      'country': instance.country,
      'currency': instance.currency,
      'releaseDate': instance.releaseDate?.toIso8601String(),
      'primaryGenreName': instance.primaryGenreName,
    };

// **************************************************************************
// RealmObjectGenerator
// **************************************************************************

class AlbumsDataInRealm extends _AlbumsDataInRealm
    with RealmEntity, RealmObjectBase, RealmObject {
  AlbumsDataInRealm({
    String? wrapperType,
    String? collectionType,
    int? artistId,
    int? collectionId,
    int? amgArtistId,
    String? artistName,
    String? collectionName,
    String? collectionCensoredName,
    String? artistViewUrl,
    String? collectionViewUrl,
    String? artworkUrl60,
    String? artworkUrl100,
    double? collectionPrice,
    String? collectionExplicitness,
    int? trackCount,
    String? copyright,
    String? country,
    String? currency,
    DateTime? releaseDate,
    String? primaryGenreName,
  }) {
    RealmObjectBase.set(this, 'wrapperType', wrapperType);
    RealmObjectBase.set(this, 'collectionType', collectionType);
    RealmObjectBase.set(this, 'artistId', artistId);
    RealmObjectBase.set(this, 'collectionId', collectionId);
    RealmObjectBase.set(this, 'amgArtistId', amgArtistId);
    RealmObjectBase.set(this, 'artistName', artistName);
    RealmObjectBase.set(this, 'collectionName', collectionName);
    RealmObjectBase.set(this, 'collectionCensoredName', collectionCensoredName);
    RealmObjectBase.set(this, 'artistViewUrl', artistViewUrl);
    RealmObjectBase.set(this, 'collectionViewUrl', collectionViewUrl);
    RealmObjectBase.set(this, 'artworkUrl60', artworkUrl60);
    RealmObjectBase.set(this, 'artworkUrl100', artworkUrl100);
    RealmObjectBase.set(this, 'collectionPrice', collectionPrice);
    RealmObjectBase.set(this, 'collectionExplicitness', collectionExplicitness);
    RealmObjectBase.set(this, 'trackCount', trackCount);
    RealmObjectBase.set(this, 'copyright', copyright);
    RealmObjectBase.set(this, 'country', country);
    RealmObjectBase.set(this, 'currency', currency);
    RealmObjectBase.set(this, 'releaseDate', releaseDate);
    RealmObjectBase.set(this, 'primaryGenreName', primaryGenreName);
  }

  AlbumsDataInRealm._();

  @override
  String? get wrapperType =>
      RealmObjectBase.get<String>(this, 'wrapperType') as String?;
  @override
  set wrapperType(String? value) =>
      RealmObjectBase.set(this, 'wrapperType', value);

  @override
  String? get collectionType =>
      RealmObjectBase.get<String>(this, 'collectionType') as String?;
  @override
  set collectionType(String? value) =>
      RealmObjectBase.set(this, 'collectionType', value);

  @override
  int? get artistId => RealmObjectBase.get<int>(this, 'artistId') as int?;
  @override
  set artistId(int? value) => RealmObjectBase.set(this, 'artistId', value);

  @override
  int? get collectionId =>
      RealmObjectBase.get<int>(this, 'collectionId') as int?;
  @override
  set collectionId(int? value) =>
      RealmObjectBase.set(this, 'collectionId', value);

  @override
  int? get amgArtistId => RealmObjectBase.get<int>(this, 'amgArtistId') as int?;
  @override
  set amgArtistId(int? value) =>
      RealmObjectBase.set(this, 'amgArtistId', value);

  @override
  String? get artistName =>
      RealmObjectBase.get<String>(this, 'artistName') as String?;
  @override
  set artistName(String? value) =>
      RealmObjectBase.set(this, 'artistName', value);

  @override
  String? get collectionName =>
      RealmObjectBase.get<String>(this, 'collectionName') as String?;
  @override
  set collectionName(String? value) =>
      RealmObjectBase.set(this, 'collectionName', value);

  @override
  String? get collectionCensoredName =>
      RealmObjectBase.get<String>(this, 'collectionCensoredName') as String?;
  @override
  set collectionCensoredName(String? value) =>
      RealmObjectBase.set(this, 'collectionCensoredName', value);

  @override
  String? get artistViewUrl =>
      RealmObjectBase.get<String>(this, 'artistViewUrl') as String?;
  @override
  set artistViewUrl(String? value) =>
      RealmObjectBase.set(this, 'artistViewUrl', value);

  @override
  String? get collectionViewUrl =>
      RealmObjectBase.get<String>(this, 'collectionViewUrl') as String?;
  @override
  set collectionViewUrl(String? value) =>
      RealmObjectBase.set(this, 'collectionViewUrl', value);

  @override
  String? get artworkUrl60 =>
      RealmObjectBase.get<String>(this, 'artworkUrl60') as String?;
  @override
  set artworkUrl60(String? value) =>
      RealmObjectBase.set(this, 'artworkUrl60', value);

  @override
  String? get artworkUrl100 =>
      RealmObjectBase.get<String>(this, 'artworkUrl100') as String?;
  @override
  set artworkUrl100(String? value) =>
      RealmObjectBase.set(this, 'artworkUrl100', value);

  @override
  double? get collectionPrice =>
      RealmObjectBase.get<double>(this, 'collectionPrice') as double?;
  @override
  set collectionPrice(double? value) =>
      RealmObjectBase.set(this, 'collectionPrice', value);

  @override
  String? get collectionExplicitness =>
      RealmObjectBase.get<String>(this, 'collectionExplicitness') as String?;
  @override
  set collectionExplicitness(String? value) =>
      RealmObjectBase.set(this, 'collectionExplicitness', value);

  @override
  int? get trackCount => RealmObjectBase.get<int>(this, 'trackCount') as int?;
  @override
  set trackCount(int? value) => RealmObjectBase.set(this, 'trackCount', value);

  @override
  String? get copyright =>
      RealmObjectBase.get<String>(this, 'copyright') as String?;
  @override
  set copyright(String? value) => RealmObjectBase.set(this, 'copyright', value);

  @override
  String? get country =>
      RealmObjectBase.get<String>(this, 'country') as String?;
  @override
  set country(String? value) => RealmObjectBase.set(this, 'country', value);

  @override
  String? get currency =>
      RealmObjectBase.get<String>(this, 'currency') as String?;
  @override
  set currency(String? value) => RealmObjectBase.set(this, 'currency', value);

  @override
  DateTime? get releaseDate =>
      RealmObjectBase.get<DateTime>(this, 'releaseDate') as DateTime?;
  @override
  set releaseDate(DateTime? value) =>
      RealmObjectBase.set(this, 'releaseDate', value);

  @override
  String? get primaryGenreName =>
      RealmObjectBase.get<String>(this, 'primaryGenreName') as String?;
  @override
  set primaryGenreName(String? value) =>
      RealmObjectBase.set(this, 'primaryGenreName', value);

  @override
  Stream<RealmObjectChanges<AlbumsDataInRealm>> get changes =>
      RealmObjectBase.getChanges<AlbumsDataInRealm>(this);

  @override
  AlbumsDataInRealm freeze() =>
      RealmObjectBase.freezeObject<AlbumsDataInRealm>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObjectBase.registerFactory(AlbumsDataInRealm._);
    return const SchemaObject(
        ObjectType.realmObject, AlbumsDataInRealm, 'AlbumsDataInRealm', [
      SchemaProperty('wrapperType', RealmPropertyType.string, optional: true),
      SchemaProperty('collectionType', RealmPropertyType.string,
          optional: true),
      SchemaProperty('artistId', RealmPropertyType.int, optional: true),
      SchemaProperty('collectionId', RealmPropertyType.int, optional: true),
      SchemaProperty('amgArtistId', RealmPropertyType.int, optional: true),
      SchemaProperty('artistName', RealmPropertyType.string, optional: true),
      SchemaProperty('collectionName', RealmPropertyType.string,
          optional: true),
      SchemaProperty('collectionCensoredName', RealmPropertyType.string,
          optional: true),
      SchemaProperty('artistViewUrl', RealmPropertyType.string, optional: true),
      SchemaProperty('collectionViewUrl', RealmPropertyType.string,
          optional: true),
      SchemaProperty('artworkUrl60', RealmPropertyType.string, optional: true),
      SchemaProperty('artworkUrl100', RealmPropertyType.string, optional: true),
      SchemaProperty('collectionPrice', RealmPropertyType.double,
          optional: true),
      SchemaProperty('collectionExplicitness', RealmPropertyType.string,
          optional: true),
      SchemaProperty('trackCount', RealmPropertyType.int, optional: true),
      SchemaProperty('copyright', RealmPropertyType.string, optional: true),
      SchemaProperty('country', RealmPropertyType.string, optional: true),
      SchemaProperty('currency', RealmPropertyType.string, optional: true),
      SchemaProperty('releaseDate', RealmPropertyType.timestamp,
          optional: true),
      SchemaProperty('primaryGenreName', RealmPropertyType.string,
          optional: true),
    ]);
  }
}
