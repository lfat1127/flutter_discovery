import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'itunes_search_response.g.dart';

@JsonSerializable()
class ItunesSearchResponse {
  final int resultCount;
  final List<AlbumsData> results;

  ItunesSearchResponse(this.resultCount, this.results);

  factory ItunesSearchResponse.fromJson(Map<String, dynamic> json) =>
      _$ItunesSearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ItunesSearchResponseToJson(this);
}
