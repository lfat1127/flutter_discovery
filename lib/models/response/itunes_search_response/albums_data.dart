import 'package:json_annotation/json_annotation.dart';
import 'package:realm/realm.dart';
part 'albums_data.g.dart';

@JsonSerializable()
class AlbumsData {
  final String? wrapperType;
  final String? collectionType;
  final int? artistId;
  final int? collectionId;
  final int? amgArtistId;
  final String? artistName;
  final String? collectionName;
  final String? collectionCensoredName;
  final String? artistViewUrl;
  final String? collectionViewUrl;
  final String? artworkUrl60;
  final String? artworkUrl100;
  final double? collectionPrice;
  final String? collectionExplicitness;
  final int? trackCount;
  final String? copyright;
  final String? country;
  final String? currency;
  final DateTime? releaseDate;
  final String? primaryGenreName;

  AlbumsData(
      this.wrapperType,
      this.collectionType,
      this.artistId,
      this.collectionId,
      this.amgArtistId,
      this.artistName,
      this.collectionName,
      this.collectionCensoredName,
      this.artistViewUrl,
      this.collectionViewUrl,
      this.artworkUrl60,
      this.artworkUrl100,
      this.collectionPrice,
      this.collectionExplicitness,
      this.trackCount,
      this.copyright,
      this.country,
      this.currency,
      this.releaseDate,
      this.primaryGenreName);

  factory AlbumsData.fromJson(Map<String, dynamic> json) =>
      _$AlbumsDataFromJson(json);

  Map<String, dynamic> toJson() => _$AlbumsDataToJson(this);

  const AlbumsData.empty()
      : wrapperType = "",
        collectionType = "",
        artistId = 0,
        collectionId = 0,
        amgArtistId = 0,
        artistName = "",
        collectionName = "",
        collectionCensoredName = "",
        artistViewUrl = "",
        collectionViewUrl = "",
        artworkUrl60 = "",
        artworkUrl100 = "",
        collectionPrice = 0,
        collectionExplicitness = "",
        trackCount = 0,
        copyright = "",
        country = "",
        currency = "",
        releaseDate = null,
        primaryGenreName = "";
}

@RealmModel()
class _AlbumsDataInRealm {
  late String? wrapperType;
  late String? collectionType;
  late int? artistId;
  late int? collectionId;
  late int? amgArtistId;
  late String? artistName;
  late String? collectionName;
  late String? collectionCensoredName;
  late String? artistViewUrl;
  late String? collectionViewUrl;
  late String? artworkUrl60;
  late String? artworkUrl100;
  late double? collectionPrice;
  late String? collectionExplicitness;
  late int? trackCount;
  late String? copyright;
  late String? country;
  late String? currency;
  late DateTime? releaseDate;
  late String? primaryGenreName;
}

extension AlbumsDataRealmObject on AlbumsData {
  AlbumsDataInRealm convertToRealmObject() {
    return AlbumsDataInRealm(
      wrapperType: wrapperType,
      collectionType: collectionType,
      artistId: artistId,
      collectionId: collectionId,
      amgArtistId: amgArtistId,
      artistName: artistName,
      collectionName: collectionName,
      collectionCensoredName: collectionCensoredName,
      artistViewUrl: artistViewUrl,
      collectionViewUrl: collectionViewUrl,
      artworkUrl60: artworkUrl60,
      artworkUrl100: artworkUrl100,
      collectionPrice: collectionPrice,
      collectionExplicitness: collectionExplicitness,
      trackCount: trackCount,
      copyright: copyright,
      country: country,
      currency: currency,
      releaseDate: releaseDate,
      primaryGenreName: primaryGenreName,
    );
  }
}
