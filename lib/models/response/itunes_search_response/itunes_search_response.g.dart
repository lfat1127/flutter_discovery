// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'itunes_search_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItunesSearchResponse _$ItunesSearchResponseFromJson(
        Map<String, dynamic> json) =>
    ItunesSearchResponse(
      json['resultCount'] as int,
      (json['results'] as List<dynamic>)
          .map((e) => AlbumsData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ItunesSearchResponseToJson(
        ItunesSearchResponse instance) =>
    <String, dynamic>{
      'resultCount': instance.resultCount,
      'results': instance.results,
    };
