import 'package:flutter/material.dart';
import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:flutter_experience/view/details/details_view.dart';
import 'package:flutter_experience/view/main/main_view.dart';
import 'package:go_router/go_router.dart';

// GoRouter configuration
final flutterExperienceRouter = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const MainView(),
    ),
    GoRoute(
        path: '/details',
        pageBuilder: (context, state) {
          AlbumsData? albumsData;
          try {
            if (state.extra != null && state.extra is AlbumsData) {
              albumsData = state.extra as AlbumsData;
            }
          } catch (_) {
            albumsData = null;
          }

          return CustomTransitionPage<void>(
            child: DetailsView(albumsData: albumsData),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              // modal transition
              const begin = Offset(0.0, 1.0);
              const end = Offset.zero;
              const curve = Curves.ease;

              var tween =
                  Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

              return SlideTransition(
                position: animation.drive(tween),
                child: child,
              );
            },
          );
        })
  ],
);
