extension NullHanlding on double? {
  double returnZeroIfNull() {
    return this ?? 0.0;
  }
}
