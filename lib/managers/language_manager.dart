import 'package:flutter/material.dart';
import 'package:flutter_experience/enums/shared_preference/shared_preference_key.dart';
import 'package:flutter_experience/generated/l10n.dart';
import 'package:flutter_experience/managers/shared_preference_manager.dart';
import 'package:rxdart/rxdart.dart';

class LanguageManager {
  static final LanguageManager _singleton = LanguageManager._internal();

  factory LanguageManager() => _singleton;

  LanguageManager._internal();

  final defaultLanguageCode = "en";

  final currentLanguage = BehaviorSubject.seeded(const Locale('en'));

  final sharedPreferenceManager = SharedPreferenceManager();

  ensureInitialized() async {
    final savedCurrentLanguage = await sharedPreferenceManager.getStringValue(
        key: SharedPreferenceKey.currentLanguage, defaultValue: 'en');
    currentLanguage.add(Locale(savedCurrentLanguage));
  }

  changeLanguageTo(
      {required BuildContext context, required String currentLanguageCode}) {
    if (currentLanguageCode == "en") {
      S.load(const Locale('zh'));
      sharedPreferenceManager.saveStringValue(
          key: SharedPreferenceKey.currentLanguage, value: 'zh');
      currentLanguage.add(const Locale('zh'));
    } else {
      S.load(const Locale('en'));
      sharedPreferenceManager.saveStringValue(
          key: SharedPreferenceKey.currentLanguage, value: 'en');
      currentLanguage.add(const Locale('en'));
    }
  }
}
