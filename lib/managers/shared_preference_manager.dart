import 'package:flutter/foundation.dart';
import 'package:flutter_experience/enums/shared_preference/shared_preference_key.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceManager {
  static final SharedPreferenceManager _singleton =
      SharedPreferenceManager._internal();

  factory SharedPreferenceManager() => _singleton;

  SharedPreferenceManager._internal();

  Future<String> getStringValue(
      {required SharedPreferenceKey key, required String defaultValue}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return Future.value(prefs.getString(key.name) ?? defaultValue);
  }

  Future<bool> saveStringValue(
      {required SharedPreferenceKey key, required String value}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key.name, value);
  }
}
