import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_experience/enums/shared_preference/shared_preference_key.dart';
import 'package:flutter_experience/managers/shared_preference_manager.dart';
import 'package:rxdart/rxdart.dart';

class UserSettingsManager {
  static final UserSettingsManager _singleton = UserSettingsManager._internal();

  factory UserSettingsManager() => _singleton;

  UserSettingsManager._internal();

  final _sharedPreferenceManager = SharedPreferenceManager();

  final BehaviorSubject<ThemeMode> themeModeSubject =
      BehaviorSubject.seeded(ThemeMode.system);

  ensureInitialized() async {
    final savedThemeMode = await _sharedPreferenceManager.getStringValue(
        key: SharedPreferenceKey.themeMode, defaultValue: 'system');
    ThemeMode themeMode = ThemeMode.system;
    try {
      themeMode = ThemeMode.values
          .where((element) => element.name == savedThemeMode)
          .first;
    } catch (_) {
      themeMode = ThemeMode.system;
    }
    themeModeSubject.add(themeMode);
  }

  Future<bool> changeThemeMode({required ThemeMode themeMode}) {
    themeModeSubject.add(themeMode);
    return _sharedPreferenceManager.saveStringValue(
        key: SharedPreferenceKey.themeMode, value: themeMode.name);
  }
}
