part of 'details_bloc.dart';

abstract class DetailsEvent extends Equatable {
  const DetailsEvent();

  @override
  List<Object> get props => [];
}

class InitialiseEvent extends DetailsEvent {
  final AlbumsData? albumsData;
  const InitialiseEvent({required this.albumsData});
}

class BookmarkTappedEvent extends DetailsEvent {
  const BookmarkTappedEvent();
}
