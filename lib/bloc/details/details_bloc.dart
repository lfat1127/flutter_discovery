import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:flutter_experience/repository/local_db_repository.dart';

part 'details_event.dart';
part 'details_state.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  final LocalDatabaseRepository localDatabaseRepository;
  DetailsBloc({required this.localDatabaseRepository})
      : super(const DetailsState.initial()) {
    on<InitialiseEvent>(_handleInitialiseEvent);
    on<BookmarkTappedEvent>(_handleBookmarkTappedEvent);
  }

  void _handleInitialiseEvent(
      InitialiseEvent event, Emitter<DetailsState> emit) {
    if (event.albumsData != null) {
      final savedAlbums = localDatabaseRepository.getSavedAlbums();
      emit(state.copyWith(
          albumsData: event.albumsData,
          isBookmarked: savedAlbums
              .where((element) =>
                  element.collectionViewUrl ==
                  event.albumsData?.collectionViewUrl)
              .isNotEmpty));
    }
  }

  void _handleBookmarkTappedEvent(
      BookmarkTappedEvent event, Emitter<DetailsState> emit) {
    if (state.isBookmarked && state.albumsData.collectionViewUrl != null) {
      localDatabaseRepository
          .removeFromLocal(state.albumsData.collectionViewUrl!);
      emit(state.copyWith(isBookmarked: false));
    } else {
      localDatabaseRepository.saveToLocal(state.albumsData);
      emit(state.copyWith(isBookmarked: true));
    }
  }
}
