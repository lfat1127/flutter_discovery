part of 'details_bloc.dart';

class DetailsState extends Equatable {
  final AlbumsData albumsData;
  final bool isBookmarked;
  const DetailsState({required this.albumsData, required this.isBookmarked});

  const DetailsState.initial()
      : albumsData = const AlbumsData.empty(),
        isBookmarked = false;

  DetailsState copyWith({
    final AlbumsData? albumsData,
    final bool? isBookmarked,
  }) =>
      DetailsState(
        albumsData: albumsData ?? this.albumsData,
        isBookmarked: isBookmarked ?? this.isBookmarked,
      );

  @override
  List<Object> get props => [albumsData, isBookmarked];
}
