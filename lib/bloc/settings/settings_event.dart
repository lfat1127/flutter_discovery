part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];
}

class InitialiseEvent extends SettingsEvent {
  const InitialiseEvent();
}

class ThemeModeSwitchedEvent extends SettingsEvent {
  const ThemeModeSwitchedEvent();
}
