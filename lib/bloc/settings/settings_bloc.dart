import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_experience/managers/user_settings_manager.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc() : super(const SettingsState.initial()) {
    on<InitialiseEvent>(_handleInitialiseEvent);
    on<ThemeModeSwitchedEvent>(_handleThemeModeSwitchedEvent);
  }

  _handleInitialiseEvent(InitialiseEvent event, Emitter<SettingsState> emit) {
    emit(state.copyWith(
        isDarkMode:
            UserSettingsManager().themeModeSubject.value == ThemeMode.dark));
  }

  _handleThemeModeSwitchedEvent(
      ThemeModeSwitchedEvent event, Emitter<SettingsState> emit) async {
    final themeModeChanged = await UserSettingsManager().changeThemeMode(
        themeMode: state.isDarkMode ? ThemeMode.light : ThemeMode.dark);
    if (themeModeChanged) {
      emit(state.copyWith(isDarkMode: !state.isDarkMode));
    }
  }
}
