part of 'bookmarked_bloc.dart';

@immutable
class BookmarkedState extends Equatable {
  final List<AlbumsDataInRealm> albumDatasInRealm;
  const BookmarkedState({required this.albumDatasInRealm});

  const BookmarkedState.initial() : albumDatasInRealm = const [];

  BookmarkedState copyWith({
    List<AlbumsDataInRealm>? albumDatasInRealm,
  }) =>
      BookmarkedState(
        albumDatasInRealm: albumDatasInRealm ?? this.albumDatasInRealm,
      );

  @override
  List<Object> get props => [albumDatasInRealm];
}
