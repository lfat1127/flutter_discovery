import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:flutter_experience/repository/local_db_repository.dart';

part 'bookmarked_event.dart';
part 'bookmarked_state.dart';

class BookmarkedBloc extends Bloc<BookmarkedEvent, BookmarkedState> {
  final LocalDatabaseRepository localDatabaseRepository;
  BookmarkedBloc({required this.localDatabaseRepository})
      : super(const BookmarkedState.initial()) {
    on<InitialiseEvent>(_handleInitialiseEvent);
    on<RemoveBookmarkEvent>(_handleRemoveBookmarkEvent);
  }

  void _handleInitialiseEvent(
      InitialiseEvent event, Emitter<BookmarkedState> emit) {
    final bookmarkedAlbumsInRealmResult =
        localDatabaseRepository.getSavedAlbums();
    emit(state.copyWith(
        albumDatasInRealm: bookmarkedAlbumsInRealmResult.toList()));
  }

  void _handleRemoveBookmarkEvent(
      RemoveBookmarkEvent event, Emitter<BookmarkedState> emit) {
    localDatabaseRepository.removeFromLocal(event.collectionViewUrl);
    final bookmarkedAlbumsInRealmResult =
        localDatabaseRepository.getSavedAlbums();
    emit(state.copyWith(
        albumDatasInRealm: bookmarkedAlbumsInRealmResult.toList()));
  }
}
