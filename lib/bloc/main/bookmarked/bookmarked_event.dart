part of 'bookmarked_bloc.dart';

@immutable
abstract class BookmarkedEvent {
  const BookmarkedEvent();
}

@immutable
class InitialiseEvent extends BookmarkedEvent {
  const InitialiseEvent();
}

@immutable
class RemoveBookmarkEvent extends BookmarkedEvent {
  final String collectionViewUrl;
  const RemoveBookmarkEvent({required this.collectionViewUrl});
}
