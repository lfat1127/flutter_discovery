part of 'albums_bloc.dart';

@immutable
abstract class AlbumsEvent {
  const AlbumsEvent();
}

@immutable
class InitialiseEvent extends AlbumsEvent {
  const InitialiseEvent();
}

@immutable
class BookmarkButtonTappedEvent extends AlbumsEvent {
  final AlbumsData albumData;
  const BookmarkButtonTappedEvent({required this.albumData});
}

class LocalDatabaseChangesEvent extends AlbumsEvent {
  const LocalDatabaseChangesEvent();
}

class SearchTextChangesEvent extends AlbumsEvent {
  final String keywords;
  const SearchTextChangesEvent({required this.keywords});
}
