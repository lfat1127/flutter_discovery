import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_experience/models/response/itunes_search_response/albums_data.dart';
import 'package:flutter_experience/repository/itunes_search_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_experience/repository/local_db_repository.dart';

part 'albums_event.dart';
part 'albums_state.dart';

class AlbumsBloc extends Bloc<AlbumsEvent, AlbumsState> {
  final ItunesSearchRepository itunesSearchRepository;
  final LocalDatabaseRepository localDatabaseRepository;

  AlbumsBloc({
    required this.itunesSearchRepository,
    required this.localDatabaseRepository,
  }) : super(const AlbumsState.initial()) {
    on<InitialiseEvent>(_handleInitialiseEvent);
    on<BookmarkButtonTappedEvent>(_handleBookmarkButtonTappedEvent);
    on<LocalDatabaseChangesEvent>(_handleLocalDatabaseChangesEvent);
    on<SearchTextChangesEvent>(_handleSearchTextChangesEvent);
  }

  Future<void> _handleInitialiseEvent(
      InitialiseEvent event, Emitter<AlbumsState> emit) async {
    // final results =
    //     await itunesSearchRepository.searchAlbum(term: "jack johnson");
    final bookmarkedAlbums = localDatabaseRepository.getSavedAlbums();
    final bookmarkedCollectionCollectionViewUrl =
        bookmarkedAlbums.map((e) => e.collectionViewUrl ?? "").toList();

    emit(state.copyWith(
        isLoading: false,
        // albumDatas: results,
        bookmarkedCollectionCollectionViewUrl:
            bookmarkedCollectionCollectionViewUrl));
  }

  void _handleBookmarkButtonTappedEvent(
      BookmarkButtonTappedEvent event, Emitter<AlbumsState> emit) {
    final bookmarkedCollectionCollectionViewUrl =
        List.of(state.bookmarkedCollectionCollectionViewUrl);
    if (event.albumData.collectionViewUrl != null) {
      if (bookmarkedCollectionCollectionViewUrl
          .contains(event.albumData.collectionViewUrl)) {
        localDatabaseRepository
            .removeFromLocal(event.albumData.collectionViewUrl!);
        bookmarkedCollectionCollectionViewUrl
            .remove(event.albumData.collectionViewUrl);
      } else {
        localDatabaseRepository.saveToLocal(event.albumData);
        bookmarkedCollectionCollectionViewUrl
            .add(event.albumData.collectionViewUrl ?? "");
      }
      emit(state.copyWith(
          bookmarkedCollectionCollectionViewUrl:
              bookmarkedCollectionCollectionViewUrl));
    } else {
      //TODO say error
    }
  }

  void _handleLocalDatabaseChangesEvent(
      LocalDatabaseChangesEvent event, Emitter<AlbumsState> emit) {
    final bookmarkedAlbums = localDatabaseRepository.getSavedAlbums();
    final bookmarkedCollectionCollectionViewUrl =
        bookmarkedAlbums.map((e) => e.collectionViewUrl ?? "").toList();

    emit(state.copyWith(
        isLoading: false,
        bookmarkedCollectionCollectionViewUrl:
            bookmarkedCollectionCollectionViewUrl));
  }

  Future<void> _handleSearchTextChangesEvent(
      SearchTextChangesEvent event, Emitter<AlbumsState> emit) async {
    final results =
        await itunesSearchRepository.searchAlbum(term: event.keywords);
    emit(state.copyWith(
      isLoading: false,
      albumDatas: results,
    ));
  }
}
