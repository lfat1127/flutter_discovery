part of 'albums_bloc.dart';

@immutable
class AlbumsState extends Equatable {
  final bool isLoading;
  final List<AlbumsData> albumDatas;
  final List<String> bookmarkedCollectionCollectionViewUrl;

  const AlbumsState.initial()
      : isLoading = true,
        albumDatas = const [],
        bookmarkedCollectionCollectionViewUrl = const [];

  const AlbumsState({
    required this.isLoading,
    required this.albumDatas,
    required this.bookmarkedCollectionCollectionViewUrl,
  });

  AlbumsState copyWith({
    bool? isLoading,
    List<AlbumsData>? albumDatas,
    List<String>? bookmarkedCollectionCollectionViewUrl,
  }) =>
      AlbumsState(
          isLoading: isLoading ?? this.isLoading,
          albumDatas: albumDatas ?? this.albumDatas,
          bookmarkedCollectionCollectionViewUrl:
              bookmarkedCollectionCollectionViewUrl ??
                  this.bookmarkedCollectionCollectionViewUrl);

  @override
  List<Object> get props =>
      [isLoading, albumDatas, bookmarkedCollectionCollectionViewUrl];
}
