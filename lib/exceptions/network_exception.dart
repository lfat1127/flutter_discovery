class NetworkException implements Exception {
  final unknownStatusCode = 999;
  late int statusCode;

  NetworkException(int? statusCode) {
    this.statusCode = statusCode ?? unknownStatusCode;
  }
}
