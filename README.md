# flutter_experience

A Flutter project collects packages together with BLOC pattern.

## Getting Started

This project is a kind of study on FLutter and learnt while I'm working on Keychain Pay app and studying what design pattern and packages should be on FLutter application.

A few resources to get you started:

- [API will use for this project](https://itunes.apple.com/search?term=Bruno%20Mars&entity=album)
- [Package - flutter_bloc](https://pub.dev/packages/flutter_bloc) for design pattern
- [Package - bloc](https://pub.dev/packages/bloc) for design pattern
- [Package - equatable](https://pub.dev/packages/equatable) for bloc
- [Package - json_annotation](https://pub.dev/packages/json_annotation) for api result which reply in json format
- [Package - realm](https://pub.dev/packages/realm) for local storage
- [Package - dio](https://pub.dev/packages/dio) for api request
- [Package - go_router](https://pub.dev/packages/go_router) for navigation
- [Package - cached_network_image](https://pub.dev/packages/cached_network_image) for download and cache image from network
- [Package - auto_size_text](https://pub.dev/packages/auto_size_text) for auto size the text
- [Package - rxdart](https://pub.dev/packages/rxdart) for boardcast and receive some event status
- [Package - shared_preferences](https://pub.dev/packages/shared_preferences) for storage simple key value like user's settings.

** Tested only on mobile app. For other OS target like mac, windows or web, this app may not be able to run.
